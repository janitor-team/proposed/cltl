# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the cltl package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: cltl\n"
"Report-Msgid-Bugs-To: cltl@packages.debian.org\n"
"POT-Creation-Date: 2018-12-11 15:00+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Download the cltl book from the Internet?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"You don't have the file /var/cache/cltl/cltl_ht.tar.gz. You may want to "
"download this file from internet now and proceed with the installation "
"afterward."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Unable to download. Try again?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"An error occurred during the download of book from the Internet. You may now "
"request to try the download again."
msgstr ""
