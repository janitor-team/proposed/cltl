cltl (1.0.32) unstable; urgency=medium

  * Team upload
  * Update Spanish translation. Thanks to Camaleón. (Closes: #962053)
  * d/control: mark XS-Autobuild: yes
  * Trim trailing whitespace.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 20 Dec 2021 15:59:11 +0100

cltl (1.0.31) unstable; urgency=medium

  * Team upload
  * Fix permissions of /usr/share/doc/cltl/clm/index.html, on new installations
    and on upgrades. (Closes: #947772)
  * Update Swedish translation. Thanks to Martin Bagge. (Closes: #918016)
  * Update Danish translation. Thanks to Joe Hansen. (Closes: #923145)
  * Bump to debhelper compat level 12
  * Bump S-V to 4.4.1

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 19 Jan 2020 22:06:57 +0100

cltl (1.0.30) unstable; urgency=medium

  * Team upload

  [ Andreas Beckmann ]
  * preinst: Disable buggy postrm code on upgrades from (<< 1.0.27) that would
    delete files from the newly unpacked package.  (Cf. #700670)
  * Clean up the obsolete conffile /etc/emacs/site-start.d/60cltl.el on
    upgrade.
  * Use /var/cache/cltl as download location.
    (Closes: #916028)

  [ Sébastien Villemot ]
  * Update French translation. Thanks to Jean-Philippe Mengual.
    (Closes: #916082)
  * Add Rules-Requires-Root: no
  * Use new way of specifying debhelper compat level
  * Bump S-V to 4.2.1

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 11 Dec 2018 15:39:10 +0100

cltl (1.0.29) unstable; urgency=medium

  * Team upload.
  * Update Vcs-* fields for move to salsa.
  * Set Maintainer to debian-common-lisp@l.d.o.
  * Update Russian translation of debconf template. Thanks to Lev Lamberov.
    (Closes: #885992)
  * Bump Standards-Version to 4.1.4.
  * No longer use recursive chown/chmod; replace them by tar options
    --no-same-owner and --no-same-permissions.
  * Rewrite d/copyright using machine-readable format 1.0.

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 14 Apr 2018 10:15:35 +0200

cltl (1.0.28) unstable; urgency=medium

  * Team upload.
  * Run debconf-updatepo in the clean rule.
  * Translation updates:
    - Dutch (Frans Spiesschaert) (Closes: #881407)
    - German (Helge Kreutzmann) (Closes: #882795)
  * Bump to debhelper compat level 11.
  * Bump Standards-Version to 4.1.3.
  * d/config: remove kludge with "set +e".
  * d/postinst:
    - add "--compression=none" to wget, to prevent it from decompressing the
      archive
    - do not enter an infinite loop in non-interactive mode when download fails
    - remove kludge with "set +e"

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 29 Dec 2017 16:50:16 +0100

cltl (1.0.27) unstable; urgency=low

  * Team upload.

  [ Peter Van Eynde ]
  * Don't delete everything under /usr/share/doc/cltl/
    (Closes: #700670)
  * Added Dutch translation of debconf templates
    (Closes: #675316)
  * Added Turkish translation of debconf template
    (Closes: #757866)
  * Go to debhelper compat level 10
  * Use dh_installmanpages instead of dh_installman
  * Fix Typo in debconf message
    (Closes: #660969)

  [ Sébastien Villemot ]
  * Delete obsolete README.building.
  * Add Vcs-Browser field.
  * Bump to Standards-Version 4.1.1.
  * Fix mailing list address for Chinese translation.
  * Remove obsolete stuff about 60cltl.el script.
  * Remove incorrect postrm script.

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 25 Oct 2017 21:48:40 +0200

cltl (1.0.26) unstable; urgency=low

  * Updated Standard-Version no real changes.
  * Now use debhelper v7
  * Fixed doc-base file
  * Add "set -e" for postrm
  * Fixed chown call to use root:root
  * Removed the cltl/aborting template
  * Fixed typo in package description
  * Added gl.po Closes: #511835
  * Added ru.po Closes: #512668

 -- Peter Van Eynde <pvaneynd@debian.org>  Wed, 02 Sep 2009 16:13:31 +0100

cltl (1.0.25) unstable; urgency=low

  * Changed to group maintanance
  * Corrected Vcs-Git control field
  * swap binary-indep and binary-arch
  * Updated standard version without real changes

 -- Peter Van Eynde <pvaneynd@debian.org>  Sat, 23 Feb 2008 14:50:46 +0100

cltl (1.0.24) unstable; urgency=low

  * Added German debconf translation (Closes: #412216)

 -- Peter Van Eynde <pvaneynd@debian.org>  Wed, 28 Feb 2007 13:19:03 +0100

cltl (1.0.23) unstable; urgency=low

  * Included Spanish po-debconf translation (Closes: #407056)
  * Added build dependency for po-debconf

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue, 23 Jan 2007 06:59:18 +0100

cltl (1.0.22) unstable; urgency=low

  * Run debconf-updatepo in the clean target to ensure uptodate PO
    files. (from lintian)
  * Added simplify Chinese translation for cltl (Closes: #404579)

 -- Peter Van Eynde <pvaneynd@debian.org>  Sat, 30 Dec 2006 00:53:25 +0100

cltl (1.0.21) unstable; urgency=low

  * Added XS-X-Vcs-Darcs header
  * modified S-X-Vcs-Darcs to XS-Vcs-Darcs field
  * Include Portuguese translation for debconf (Closes: #389653)

 -- Peter Van Eynde <pvaneynd@debian.org>  Mon,  9 Oct 2006 10:42:16 +0200

cltl (1.0.20) unstable; urgency=low

  * set cltl2-root-url at top level (Closes: #369604)
  * Move debhelper to Build-Depends
  * Update standards version without real changes

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue, 18 Jul 2006 07:21:06 +0200

cltl (1.0.19) unstable; urgency=low

  * Fixed speling error in description.
    Closes: #328855
  * Added swedish translation. Closes: #330347

 -- Peter Van Eynde <pvaneynd@debian.org>  Thu, 29 Sep 2005 05:12:59 +0200

cltl (1.0.18) unstable; urgency=low

  * Added italian debconf translations.
    Closes: #325626

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue, 30 Aug 2005 10:57:20 +0200

cltl (1.0.17) unstable; urgency=low

  * added ${misc:Depends} to fix debconf2 and cdebconf problems.

 -- Peter Van Eynde <pvaneynd@debian.org>  Wed,  3 Aug 2005 10:59:27 +0200

cltl (1.0.16) unstable; urgency=low

  * Includes Czech po file from Martin Šín, Closes: #319578
  * Now uses darcs-buildpackage
  * Updated Standard Version

 -- Peter Van Eynde <pvaneynd@debian.org>  Sat, 30 Jul 2005 23:16:23 +0200

cltl (1.0.15) unstable; urgency=low

  * New maintainer. (Closes: #297413: O: cltl -- Common Lisp the
    Language, second edition, book (Pre-ANSI))
  * Adopted by Peter Van Eynde

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue,  5 Jul 2005 07:51:56 +0200

cltl (1.0.14) unstable; urgency=low

  * Add Japanese translation thanks to Hideki Yamane (closes: 279956)

 -- Kevin M. Rosenberg <kmr@debian.org>  Thu, 23 Dec 2004 19:50:53 -0700

cltl (1.0.13) unstable; urgency=low

  * Update french translation (closes:244079)

 -- Kevin M. Rosenberg <kmr@debian.org>  Fri, 16 Apr 2004 09:18:21 -0600

cltl (1.0.12) unstable; urgency=low

  * Add french translation (closes:242891)

 -- Kevin M. Rosenberg <kmr@debian.org>  Fri,  9 Apr 2004 10:57:38 -0600

cltl (1.0.11) unstable; urgency=low

  * Add Danish translation (closes:240813)
  * Fix error status on upgrade (closes:240821)

 -- Kevin M. Rosenberg <kmr@debian.org>  Mon, 29 Mar 2004 09:10:42 -0700

cltl (1.0.10) unstable; urgency=low

  * Use po-debconf (closes:239868)

 -- Kevin M. Rosenberg <kmr@debian.org>  Thu, 25 Mar 2004 06:17:27 -0700

cltl (1.0.9) unstable; urgency=low

  * Move asking of download question to config and move preinst loops
  to postinst (closes:187213)

 -- Kevin M. Rosenberg <kmr@debian.org>  Wed,  2 Apr 2003 13:53:06 -0700

cltl (1.0.8) unstable; urgency=low

  * Add code to preinst to ensure old config file is removed
  (closes: 168117)

 -- Kevin M. Rosenberg <kmr@debian.org>  Thu,  7 Nov 2002 09:37:14 -0700

cltl (1.0.7) unstable; urgency=low

  * prefix,postinst: Remove bashisms

 -- Kevin M. Rosenberg <kmr@debian.org>  Sun, 22 Sep 2002 18:33:30 -0600

cltl (1.0.6) unstable; urgency=low

  * Fix syntax for ilisp key binding
  * Rename cltl-ilisp.el to cltl.el

 -- Kevin M. Rosenberg <kmr@debian.org>  Sun, 22 Sep 2002 17:10:52 -0600

cltl (1.0.5) unstable; urgency=low

  * Change function for setting ilisp key binding

 -- Kevin M. Rosenberg <kmr@debian.org>  Sun, 22 Sep 2002 09:37:50 -0600

cltl (1.0.4) unstable; urgency=low

  * Change numbering scheme since this is a native Debian package.

 -- Kevin M. Rosenberg <kmr@debian.org>  Thu, 22 Aug 2002 10:11:21 -0600

cltl (1.0-3) unstable; urgency=low

  * Add emacs startup hook for ilisp to set cltl directory.
  * Add ilisp as a Suggests.

 -- Kevin M. Rosenberg <kevin@rosenberg.net>  Sun, 11 Aug 2002 22:01:53 -0600

cltl (1.0-2) unstable; urgency=low

  * Clean-up prerm script

 -- Kevin M. Rosenberg <kmr@debian.org>  Thu, 18 Jul 2002 20:52:06 -0600

cltl (1.0-1) unstable; urgency=low

  * Initial Release (closes: 152672)

 -- Kevin M. Rosenberg <kmr@debian.org>  Sun, 14 Jul 2002 16:10:01 -0600
